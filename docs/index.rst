##################
Welcome to StatIT user manuals
##################

The statIT app helps you to analyze your i-doit system.

------------

This documentation is organized into a couple of sections:

* :ref:`about-docs`
* :ref:`introduction-docs`
* :ref:`gettingstarted-docs`

.. _about-docs:

.. toctree::
    :maxdepth: 2
    :caption: About

    about

.. _introduction-docs:

.. toctree::
    :maxdepth: 2
    :caption: Introduction

    architecture
    requirements
    installation
    configuration

.. _gettingstarted-docs:

.. toctree::
    :maxdepth: 2
    :caption: Getting Started

    overview
    graphview
    dynamicreport


##################
License
##################

`becon`_ © 2013-2018 becon GmbH

.. _becon: LICENSE.html