##################
Configuration
##################

After installing StatIT App, you need to set up your connector information.
Click on "Settings" -> "Connector" and add your i-doit url and apikey information.

.. image:: img/app_settings.jpg
    :height: 600px
    :alt: alternate text
    :align: center
