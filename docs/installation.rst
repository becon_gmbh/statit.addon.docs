##################
Installation
##################

Getting the app from the `android playstore`_.
.. _android playstore: https://play.google.com/store/apps/details?id=becon.stat_it

After installation you will see the home screen of this app.

.. image:: img/app_homescreen.jpg
    :height: 600px
    :alt: home screen
    :align: center

